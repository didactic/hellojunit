package fr.artem.melnyk.junit5.testjunit5;

public interface Greeting {
	String greet(String name);

}
