package fr.artem.melnyk.junit5.testjunit5;
import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class GreetingImplTest {

	private Greeting greeting;

	@Before
	public void setup() {
		System.out.println("Setup");
		greeting = new GreetingImpl();
	}

	@Test
	public void GreetShouldReturnValidOutput() {
		System.out.println("GreetShouldReturnValidOutput");
		String result = greeting.greet("Junit");
		assertNotNull(result);
		assertEquals("Hello Junit", result);
	}

	@Test(expected = IllegalArgumentException.class)
	public void greetShouldThrowAnException_For_NameIsNull() {
		System.out.println("greetShouldThrowAnException_For_NameIsNull");
		greeting.greet(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void greetShouldThrowAnException_For_NameIsBlanc() {
		System.out.println("greetShouldThrowAnException_For_NameIsBlanc");
		greeting.greet("");
	}

	@After
	public void tearDown() {
		System.out.println("Tear Down");
		greeting = null;
	}
}

